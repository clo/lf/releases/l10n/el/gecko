# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#filter emptyLines

# LOCALIZATION NOTE: The 'en-US' strings in the URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're
# live.

#define bookmarks_title Σελιδοδείκτες
#define bookmarks_heading Σελιδοδείκτες

#define bookmarks_toolbarfolder Εργαλειοθήκη σελιδοδεικτών
#define bookmarks_toolbarfolder_description Προσθέστε σελιδοδείκτες σε αυτό τον φάκελο για να εμφανιστούν στην εργαλειοθήκη των σελιδοδεικτών

# LOCALIZATION NOTE (getting_started):
# link title for https://www.mozilla.org/en-US/firefox/central/
#define getting_started Ξεκινώντας

# LOCALIZATION NOTE (firefox_heading):
# Firefox links folder name

# LOCALIZATION NOTE (firefox_help):
# link title for https://www.mozilla.org/en-US/firefox/help/
#define firefox_help Βοήθεια και τεκμηρίωση

# LOCALIZATION NOTE (firefox_customize):
# link title for https://www.mozilla.org/en-US/firefox/customize/
#define firefox_customize Προσαρμόστε τον Firefox

# LOCALIZATION NOTE (firefox_community):
# link title for https://www.mozilla.org/en-US/contribute/
#define firefox_community Αναμιχθείτε και εσείς

# LOCALIZATION NOTE (firefox_about):
# link title for https://www.mozilla.org/en-US/about/
#define firefox_about Πληροφορίες για εμάς

#unfilter emptyLines
